<?php

return [

    /*
    * todas las rutas generadas por el paquete estaran accesibles en
    */
    'route_prefix' => 'api',
    /*
    * si es null devolvera un error 401 si trata de entrar en alguna ruta que requiera
    * tener sesien iniciada, si es un url redigira directamente a ella
    */
    'login_url' => null,
    /*
    * añade a las rutas de la api los middlewares
    */
    'api_middlewares' => [],
    /*
    * Ruta que muestra el formulario de resetear contraseña
    */
    'reset_password_url' => 'http://google.es',
    'table_names' => [
        //Nombre de la tabla de empresas
        'companies' => '01_companies',
        //Nombre de la tabla de usuarios
        'users' => '02_users',
        //Nombre de la tabla con la relacion usuarios-empresas
        'companies_users' => '03_companies_users',
        //Nombre de la tabla con los roles
        'roles' => '04_roles',
        //Nombre de la tabla con los roles
        'permissions' => '05_permissions'
    ],
    'models' => [
        //Ruta para importar la clase utilizada para el modelo de usuarios
        'user' => 'Simbiosys\ApiBase\Model\User',
        //Ruta para importar la clase utilizada para el modelo de empresas
        'company' => 'Simbiosys\ApiBase\Model\Company',
        //Ruta para importar la clase utilizada para el modelo de roles
        'role' => 'Simbiosys\ApiBase\Model\Role',
        //Ruta para importar la clase utilizada para el modelo de permiso
        'permission' => 'Simbiosys\ApiBase\Model\Permission'
    ],
    'controllers' => [
        //Ruta para importar la clase utilizada para el controlador de usuarios
        'user' => 'Simbiosys\ApiBase\Http\Controllers\UserController',
        //Ruta para importar la clase utilizada para el controlador de empresas
        'company' => 'Simbiosys\ApiBase\Http\Controllers\CompanyController',
        //Ruta para importar la clase utilizada para el controlador de roles
        'role' => 'Simbiosys\ApiBase\Http\Controllers\RoleController',
        //Ruta para importar la clase utilizada para el controlador de permisos
        'permission' => 'Simbiosys\ApiBase\Http\Controllers\PermissionController',
        //Ruta para importar la clase utilizada para el controlador de auth
        'auth' => 'Simbiosys\ApiBase\Http\Controllers\BaseAuthController',
        //Ruta para importar la clase utilizada para el controlador de reset password
        'reset' => 'Simbiosys\ApiBase\Http\Controllers\BaseResetPasswordController'
    ],
];
