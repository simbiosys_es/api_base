# Simbiosys\ApiBase

## Requisitos

Laravel 5.6 o superior

## Instalación

* En el 'composer.json' del proyecto añadir: 

```json
"repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/simbiosys_es/api_base.git"
        }
],
"require": {
    //...
    "simbiosys_es/api_base": "v2.x-dev"
}
```

Para usarlo en local es más comodo tener ssh configurado para BitBucket, [tutorial](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html).

*************** ATENCIÓN ********************
Si da el error " array_merge(): Argument #2 is not an array ", probar a hacer "php artisan clear-co" o "php artisan config:cache" porque probablemente es que este cacheando (y si no, sacarlo a una variable)

* Ejecutar:
    * `composer update`
* Borrar los ficheros con las migraciones por defecto de Laravel
* Ejecutar:
    * `php artisan vendor:publish --provider="Simbiosys\ApiBase\ApiBaseServiceProvider"`
    * `php artisan migrate`
* Cambiar dentro de 'config/auth.php' en providers.users.model 'App\User' por config('apibase.models.user')
* Instalar Passport https://laravel.com/docs/5.7/passport
* Cambiar dentro de 'config/auth.php' el driver de la api por Passport
```
'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],

        'api' => [
            'driver' => 'passport',
            'provider' => 'users',
        ],
    ],
```

## Style guide

[PSR-2: Coding Style Guide](https://www.php-fig.org/psr/psr-2/)

## Diagramas

### Diagrama del modelo

[![diagrama-modelo](https://i.imgur.com/73fTaBx.png)](https://i.imgur.com/73fTaBx.png))

### Diagrama de clases

[![diagrama-clases](https://i.imgur.com/CCrzoEG.png)](https://i.imgur.com/CCrzoEG.png)

## Rutas

En la carpeta doc se incluye un swagger, que se puede ver <https://editor.swagger.io/>, en  con toda la información y un fichero de postman con todas las rutas

### Rutas básicas

| Method    | URI                              |
|:---------:|----------------------------------|
| POST      | api/[elemento]                   |
| GET       | api/[elemento]                   |
| GET       | api/[elemento]/{id}              |
| PUT       | api/[elemento]/{id}              |
| DELETE    | api/[elemento]/{id}              |

**Nota:** elemento puede ser: users, companies, roles, permissions

#### Rutas de recuperar contraseña

| Method    | URI                              | Parámetros                   | Respuesta                                                       |
|:---------:|----------------------------------|------------------------------|-----------------------------------------------------------------|
| POST      | api/auth/reset                   | En el body:</br>“email” del usuario que quiere el cambio de contraseña.| “token”: token necesario para el cambio de la contraseña |
| POST      | api/auth/reset/token             | En el body:</br>“email”: del usuario</br>“password”: nueva contraseña</br>“password_confirmation”: la nueva contraseña repetida</br>“token”: el token que se generó en la llamada anterior. | Si se cambio la contraseña:</br> {“status” : “ok”}</br>Si no se cambio la contraseña:</br>{“status” : “error” } |  

### Parámetros opcionales

| Parámetro         | Por defecto | Valores posibles                 |
|-------------------|-------------|---------------------------------|
| paginate          |    true     | **true**: pagina los resultados del listado. **false**: no página los resultados.      |
| limit             |    20       | Indica el número de elementos por página.                                              |
| sort              |             | Indica el nombre del campo por el cual se hace la ordenación.                          |
| order             |     asc     | **asc**: para ordenar de forma ascendente. **desc**: para ordenar de forma descendente |
| with              |             | Indica el nombre de las relaciones que se devuelven con cada objeto de la respuesta    |
| fields            |     *       | Indica el nombre de los campos que se devuelven en cada objeto de la respuesta         |

**Nota:** en los campos with y fields se puede pasar varios elementos separados por comas

## Búsqueda

Ejemplo de cómo realizar búsquedas en la api

### En el controlador:  
- Importar use Simbiosys\ApiBase\Http\Resources\ApiResourceCollection;
- Se añade la función 'getSearch' con los parámetros necesarios:  

```

private function getSearch($request)
    {
        return $request->only([
            'name',
            'email',
            'company',
            'custom_id'
        ]);
    }
    
```
- Se añadiría por ejemplo en el index, de esta manera: 

```
public function index(UserIndexRequest $request)
    {
        $repositoryParams = new RepositoryParams(
            $request->only(['paginate', 'limit', 'fields', 'with', 'sort', 'order'])
        );

        //filtrar
        $expenses = $this->repository->findBy(
            $this->getSearch($request),
            $repositoryParams
        );

        return (new ApiResourceCollection($expenses))
            ->additional([
                'meta' => [
                    'search' => (object) $this->getSearch($request)
                ]
            ])
            ->response()
            ->setStatusCode(200);
    }

```  
### En el repositorio:
- En el método que se quiera, añadir  
```
 $userQuery = $this->search($searchQuery, $userQuery);
```
- Por ejemplo:
```
public function findBy($searchQuery, RepositoryParams $repositoryParams)
    {
        $userQuery = $this->getQueryBuilderWithFields($repositoryParams);
        $userQuery = $this->getQueryBuilderWithWith($userQuery, $repositoryParams);
        $userQuery = $this->getQueryBuilderWithSortAndOrder($userQuery, $repositoryParams);

        $userQuery = $this->search($searchQuery, $userQuery);

        return $this->getResultsWithPaginateFromQueryBuilder($userQuery, $repositoryParams);
    }
```
- Su implementación sería algo así, dependiendo de los campos por los que se quiera poder buscar:
```
/**
     * Metodo que transforma el array de la request con los parametros de busqueda
     * en llamadas al queryBuilder de los gastos
     *
     * @param Array $searchQuery
     * @param QueryBuilder $queryBuilder
     * @return QueryBuilder
     */
    private function search($searchQuery, $queryBuilder)
    {
        if ($this->existsKeyAndIsNotNull('name', $searchQuery)) {
            $queryBuilder->where('name', 'like', '%'.$searchQuery['name'].'%');
        }
        if ($this->existsKeyAndIsNotNull('email', $searchQuery)) {
            $queryBuilder->where('email', 'like', '%'.$searchQuery['email'].'%');
        }
        if ($this->existsKeyAndIsNotNull('custom_id', $searchQuery)) {
            $queryBuilder->where('custom_id', 'like', '%'.$searchQuery['custom_id'].'%');
        }
        if ($this->existsKeyAndIsNotNull('nameOrMail', $searchQuery)) {
            $queryBuilder->where('email', 'like', '%'.$searchQuery['nameOrMail'].'%')
                ->orWhere('name', 'like', '%'.$searchQuery['nameOrMail'].'%');
        }
        if ($this->existsKeyAndIsNotNull('company', $searchQuery)) {
            $queryBuilder->whereHas('companies', function ($query) use ($searchQuery) {
                return $query->where('id', $searchQuery['company']);
            });
        }
        if ($this->existsKeyAndIsNotNull('costCenter', $searchQuery)) {
            $queryBuilder->whereHas('costCenters', function ($query) use ($searchQuery) {
                return $query->where(config('apibase.table_names.cost_centers').'.id', $searchQuery['costCenter']);
            });
        }
        return $queryBuilder;
    }
    
    private function existsKeyAndIsNotNull($key, $array)
    {
        return array_key_exists($key, $array) && isset($array[$key]);
    }
```


## Recuperar contraseña

Se puede personalizar el email que envia para la recuperación de contraseña, para ello hay que:

* Ejecutar:
    * `php artisan vendor:publish --tag=laravel-notifications`
* Editar en 'resources/views/vendor/notifications'

## Estructura código

### config

Contiene el archivo de configuración del paquete, esta oreparado para ser publicado para poder se único en cada proyecto.

### src

* **ApiBaseServiceProvider.php** es un [ServiceProvider](https://laravel.com/docs/5.6/providers) de Laravel, encargado de preparar el paquete para ser usardo en Laravel.
* **routes.php** contiene las rutas de este paquete.

### src/http

Contiene los [controladores](https://laravel.com/docs/5.6/controllers) y [peticiones](https://laravel.com/docs/5.6/requests) del paquete.

Hay un implementación abstract del controlador que utiliza los métodos de la interfaz 'RepositoryInterface' para ejecutar las operaciones básicas.
Para que exista seguridad, es necesario crear una [peticiones](https://laravel.com/docs/5.6/requests) con los métodos authorize y rules.

### src/migrations

Contiene las migraciones para crear la base de datos necesaria para los usuarios, empresas, roles y permisos.

## src/models

Contiene los modelos base de Usuario, Empresa, Role y Permiso.

## src/repositories

Contiene los repositorios base de Usuario, Empresa, Role y Permiso.

Hay una interfaz común para todos los repositorios, RepositoryInterface, que tiene definidos los metodos para obtener un listado y ver, actualizar y borrar un elemento concreto.

Luego cada elemento tiene su propia interfaz con los métodos propios, por ejemplo UserRepositoryInterface, tiene un método findByRoleId para buscar usuarios por el id del rol.

Y para finalizar, siguiendo con el ejemplo de los Usuarios, existe un DbUserRepository, que implementa la interfaz.

Como la implementacion de las operaciones basicas es comun existe un AbstractRepository del que pueden extender los repositorios.

### Añadir nuevos repositorios

Hay que añadir una linea en ApiBaseServiceProvider para indicarle a Laravel que implementacion de la interfaz queremos usar para la injeccion de dependencias.

Ejemplo, cuando pidamos UserRepositoryInterface, Laravel nos proporcionara un DbUserRepository.

```$this->app->bind('App\Repository\UserRepositoryInterface', 'App\Repository\Impl\DbUserRepository');```

## TODO

* Busquedas
* Documentar api_base (swagger y postman no de okticket 😇)

## Tips by Claudia

Cosas que me habría gustado aprender a la vez que aprendía a usar la apibase

* Cuando se quieren hacer varias llamadas (por ejemplo, la misma llamada en loop) y quieres poner un '.then' con axios cuando acaben todas, en vez de que se repita por cada loop, se puede hacer así:

```
  let promises = []
  
      this.lista.forEach( (x) => {
        promises.push(ApiClient.getClientApi().post('/api/cosa/' + this.cosa,{
          losparametrosquesean
        }))
      })
      
      Promise.all(promises).then(() => {
        Aquí se hace lo que se quiera, se lanza una notificación, se refresca la lista...
      })
```



