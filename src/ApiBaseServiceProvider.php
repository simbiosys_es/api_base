<?php

namespace Simbiosys\ApiBase;

use Illuminate\Support\ServiceProvider;

class ApiBaseServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //Publicar la configuracion del paquete
        $this->publishes([
            __DIR__.'/../config/apibase.php' => config_path('apibase.php'),
        ]);

        //if(!config('apibase') || config('apibase.routes')){
        //Cargar las rutas en la aplicacion Laravel
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        //}
        
        //if(!config('apibase') || config('apibase.migrations')){
        //Cargar las migraciones en la aplicacion Laravel
        $this->loadMigrationsFrom(__DIR__.'/migrations');
        //}
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'Simbiosys\ApiBase\Repository\UserRepositoryInterface',
            'Simbiosys\ApiBase\Repository\Impl\DbUserRepository'
        );
        $this->app->bind(
            'Simbiosys\ApiBase\Repository\RoleRepositoryInterface',
            'Simbiosys\ApiBase\Repository\Impl\DbRoleRepository'
        );
        $this->app->bind(
            'Simbiosys\ApiBase\Repository\ExpenseRepositoryInterface',
            'Simbiosys\ApiBase\Repository\Impl\DbExpenseRepository'
        );
        $this->app->bind(
            'Simbiosys\ApiBase\Repository\PermissionRepositoryInterface',
            'Simbiosys\ApiBase\Repository\Impl\DbPermissionRepository'
        );
        $this->app->bind(
            'Simbiosys\ApiBase\Repository\CompanyRepositoryInterface',
            'Simbiosys\ApiBase\Repository\Impl\DbCompanyRepository'
        );
    }
}
