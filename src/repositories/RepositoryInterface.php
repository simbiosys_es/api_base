<?php

namespace Simbiosys\ApiBase\Repository;

use Illuminate\Support\Collection;
use Simbiosys\ApiBase\Repository\Helper\RepositoryParams;

interface RepositoryInterface
{

    /**
     * Recupera el listado de elementos.
     *
     * @param RepositoryParams $repository_params
     * @return Collection<mixed>
     */
    public function getAll(RepositoryParams $repository_params);

    /**
     * Recupera la información del elemento con el identificador único proporcionado.
     *
     * @param integer $element_id
     * @param RepositoryParams $repository_params
     * @throws ModelNotFoundException
     * @return mixed
     */
    public function findById($element_id, RepositoryParams $repository_params);

    /**
     * Crea un nuevo elemento
     *
     * @param array $element_values
     * @return mixed
     */
    public function create($element_values);

    /**
     * Actualiza la información del elemento con el identificador único proporcionado.
     *
     * @param integer $element_id
     * @param array $element_new_values
     * @return mixed
     */
    public function updateById($element_id, $element_new_values);
    
    /**
     * Borra la información del elemento con el identificador único proporcionado.
     *
     * @param integer $element_id
     * @return boolean
     */
    public function deleteById($element_id);

    /**
     * Obtenemos la clase del modelo del repositorio
     *
     * @return mixed
     */
    // phpcs:ignore
    function model();
}
