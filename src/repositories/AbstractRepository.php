<?php

namespace Simbiosys\ApiBase\Repository;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

use Simbiosys\ApiBase\Repository\Helper\RepositoryParams;

abstract class AbstractRepository implements RepositoryInterface
{
    //! MÉTODOS DE REPOSITORYINTERFACE

    /**
     * Recupera el listado de elementos.
     *
     * @param RepositoryParams $repository_params
     * @return Collection<mixed>
     */
    public function getAll(RepositoryParams $repository_params)
    {
        $query = $this->getQueryBuilderWithFields($repository_params);
        $query = $this->getQueryBuilderWithWith($query, $repository_params);
        $query = $this->getQueryBuilderWithSortAndOrder($query, $repository_params);

        return $this->getResultsWithPaginateFromQueryBuilder($query, $repository_params);
    }

    /**
     * Recupera la información del elemento con el identificador único proporcionado.
     *
     * @param integer $element_id
     * @param RepositoryParams $repository_params
     * @throws ModelNotFoundException
     * @return mixed
     */
    public function findById($element_id, RepositoryParams $repository_params)
    {
        $query = $this->getQueryBuilderWithFields($repository_params);
        $query = $this->getQueryBuilderWithWith($query, $repository_params);

        return $query->byUser(Auth::user())->findOrFail($element_id);
    }

    /**
     * Crea un nuevo elemento
     *
     * @param array $element_values
     * @return mixed
     */
    public function create($element_values)
    {
        return call_user_func([$this->model(), 'create'], $element_values);
    }

    /**
     * Actualiza la información del elemento con el identificador único proporcionado.
     *
     * @param integer $element_id
     * @param array $element_new_values
     * @return mixed
     */
    public function updateById($element_id, $element_new_values)
    {
        $element = $this->findById($element_id, new RepositoryParams());

        collect($element->getFillable())
            ->each(function ($attribute_name) use ($element_new_values, $element) {

                if (array_has($element_new_values, $attribute_name)
                    && !in_array($attribute_name, $element->getGuarded()) ) {
                    $element[$attribute_name] = $element_new_values[$attribute_name];
                }
            });

        $element->save();

        return $element;
    }
    
    /**
     * Borra la información del elemento con el identificador único proporcionado.
     *
     * @param integer $element_id
     * @return boolean
     */
    public function deleteById($element_id)
    {
        return call_user_func([$this->model(), 'destroy'], $element_id);
    }

    //! FIN MÉTODOS DE REPOSITORYINTERFACE

    //! MÉTODOS AUXILIARES

    /**
     * Metodo que inicia el query builder desde el repository_params
     *
     * @param RepositoryParams $repository_params
     * @return QueryBuilder
     */
    protected function getQueryBuilderWithFields(RepositoryParams $repository_params)
    {
        if ($repository_params->hasFields()) {
            $query_builder = call_user_func([$this->model(), 'select'], $repository_params->fields);
        } else {
            $query_builder = call_user_func([$this->model(), 'select'], '*');
        }

        return $query_builder;
    }

    /**
     * Metodo para añadir a un query builder las opciones de with del repository params
     *
     * @param QueryBuilder $query_builder
     * @param RepositoryParams $repository_params
     * @return QueryBuilder
     */
    protected function getQueryBuilderWithWith($query_builder, RepositoryParams $repository_params)
    {

        if ($repository_params->hasWith()) {
            $query_builder = $query_builder->with($repository_params->with);
        }

        return $query_builder;
    }

    /**
     * Metodo para añadir a un query builder las opciones de sort y order del repository params
     *
     * @param QueryBuilder $query_builder
     * @param RepositoryParams $repository_params
     * @return QueryBuilder
     */
    protected function getQueryBuilderWithSortAndOrder($query_builder, RepositoryParams $repository_params)
    {
        if ($repository_params->sort) {
            $query_builder = $query_builder->orderBy($repository_params->sort, $repository_params->order);
        }

        return $query_builder;
    }

    /**
     * Metodo obtener los resultados del QueryBuilder
     *
     * @param QueryBuilder $query_builder
     * @param RepositoryParams $repository_params
     * @return Collection<mixed>
     */
    protected function getResultsWithPaginateFromQueryBuilder($query_builder, RepositoryParams $repository_params)
    {
        if ($repository_params->paginate) {
            return $query_builder->byUser(Auth::user())->paginate($repository_params->limit);
        }

        return $query_builder->byUser(Auth::user())->get();
    }

    /**
     * Metodo para hacer la consulta sobre una relacion
     *
     * @param QueryBuilder $query
     * @param string $relationshipName
     * @param int $relationshipId
     * @param string $relationshipIdField
     * @return QueryBuilder
     */
    protected function findByRelationship($query, $relationshipName, $relationshipId, $relationshipIdField)
    {
        return $query->whereHas($relationshipName, function ($query) use ($relationshipId, $relationshipIdField) {
            return $query->where($relationshipIdField, $relationshipId);
        });
    }

    //! FIN MÉTODOS AUXILIARES
}
