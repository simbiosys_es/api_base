<?php

namespace Simbiosys\ApiBase\Repository\Helper;

class RepositoryParams
{

    public $paginate = true;
    public $limit = 20;
    public $sort = null;
    public $order = 'asc';
    public $fields = [];
    public $with = [];

    public function __construct($config = [])
    {
        if (\array_key_exists('paginate', $config)) {
            $this->paginate = json_decode($config['paginate']);
        }
        if (\array_key_exists('limit', $config)) {
            $this->limit = intval($config['limit']);
        }
        if (\array_key_exists('sort', $config)) {
            $this->sort = $config['sort'];
        }
        if (\array_key_exists('order', $config)) {
            $this->order = $config['order'];
        }
        if (\array_key_exists('fields', $config)) {
            $this->fields = $this->getDataAsArray($config['fields']);
        }
        if (\array_key_exists('with', $config)) {
            $this->with = $this->getDataAsArray($config['with']);
        }
    }

    /**
     * Método para asignar el valor de paginate
     *
     * @param boolean $paginate
     * @return RepositoryParams
     */
    public function paginate($paginate)
    {
        $this->paginate = $paginate;
        return $this;
    }

    /**
     * Método para asignar el valor de limit
     *
     * @param integer $limit
     * @return RepositoryParams
     */
    public function limit($limit)
    {
        $this->limit = intval($limit);
        return $this;
    }

    /**
     * Método para asignar el valor de sort
     *
     * @param integer $sort
     * @return RepositoryParams
     */
    public function sort($sort)
    {
        $this->sort = $sort;
        return $this;
    }

    /**
     * Método para asignar el valor de order
     *
     * @param integer $order
     * @return RepositoryParams
     */
    public function order($order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * Método para asignar el valor de fields
     *
     * @param array<string> $fields
     * @return RepositoryParams
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
        return $this;
    }

    /**
     * Método para añadir un valor al array de fields
     *
     * @param string $field
     * @return RepositoryParams
     */
    public function addField($field)
    {
        $this->fields[] = $field;
        return $this;
    }

    /**
     * Método que devuelve si hay que filtrar los campos de las respuestas mediante fields
     *
     * @return boolean
     */
    public function hasFields()
    {
        $fields = collect($this->fields)->filter(function ($field) {
            return $field !== "";
        });

        return sizeof($fields) > 0;
    }

    /**
     * Método para asignar el valor de with
     *
     * @param array<string> $with
     * @return RepositoryParams
     */
    public function setWith($with)
    {
        $this->with = $with;
        return $this;
    }

    /**
     * Método para añadir un valor al array de with
     *
     * @param array<string> $with
     * @return RepositoryParams
     */
    public function addWith($with)
    {
        $this->with[] = $with;
        return $this;
    }

    /**
     * Método que devuelve si hay que devolver relaciones mediante el with
     *
     * @return boolean
     */
    public function hasWith()
    {
        $with = collect($this->with)->filter(function ($with) {
            return $with !== "";
        });

        return sizeof($with) > 0;
    }

    /**
     * Método para transformar un string con un delimitador en un array, si no es un array ya
     *
     * @param string $data
     * @param string $delimiter
     *
     * @return array
     */
    private function getDataAsArray($data, $delimiter = ',')
    {
        return explode($delimiter, $data);
    }
}
