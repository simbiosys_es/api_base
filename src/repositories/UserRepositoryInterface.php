<?php

namespace Simbiosys\ApiBase\Repository;

use Simbiosys\ApiBase\Repository\Helper\RepositoryParams;

interface UserRepositoryInterface extends RepositoryInterface
{
    /**
     * Metodo para obtener los usuarios que tiene un rol
     *
     * @param int $role_id
     * @param RepositoryParams $repository_params
     * @return Collection<User>
     */
    public function findByRoleId($role_id, RepositoryParams $repository_params);

    /**
     * Metodo para obtener los usuarios que tiene un rol
     *
     * @param int $company_id
     * @param RepositoryParams $repository_params
     * @return Collection<User>
     */
    public function findByCompanyId($company_id, RepositoryParams $repository_params);
}
