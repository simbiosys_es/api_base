<?php

namespace Simbiosys\ApiBase\Repository;

use Simbiosys\ApiBase\Repository\Helper\RepositoryParams;

interface CompanyRepositoryInterface extends RepositoryInterface
{
    /**
     * Método que devuelve el rol del usuario asociado al identificador unico que se pasa como parametro
     *
     * @param int $user_id
     * @param RepositoryParams $repository_params
     * @return Company
     */
    public function findByUserId($user_id, RepositoryParams $repository_params);
}
