<?php

namespace Simbiosys\ApiBase\Repository\Impl;

use Simbiosys\ApiBase\Repository\UserRepositoryInterface;
use Simbiosys\ApiBase\Repository\RepositoryInterface;
use Simbiosys\ApiBase\Repository\AbstractRepository;
use Simbiosys\ApiBase\Repository\Helper\RepositoryParams;

use Simbiosys\ApiBase\Model\User;

class DbUserRepository extends AbstractRepository implements UserRepositoryInterface
{
    /**
     * Obtenemos la clase del modelo del repositorio
     *
     * @return mixed
     */
    // phpcs:ignore
    function model()
    {
        return config('apibase.models.user');
    }

    /**
     * Metodo para obtener los usuarios que tiene un rol
     *
     * @param int $role_id
     * @param RepositoryParams $repository_params
     * @return Collection<User>
     */
    public function findByRoleId($role_id, RepositoryParams $repository_params)
    {
        $user_query = $this->getQueryBuilderWithFields($repository_params);
        $user_query = $this->getQueryBuilderWithWith($user_query, $repository_params);
        $user_query = $this->getQueryBuilderWithSortAndOrder($user_query, $repository_params);
        $user_query = $user_query->where('id_role', $role_id);

        return $this->getResultsWithPaginateFromQueryBuilder($user_query, $repository_params);
    }

    /**
     * Metodo para obtener los usuarios que tiene un rol
     *
     * @param int $company_id
     * @param RepositoryParams $repository_params
     * @return Collection<User>
     */
    public function findByCompanyId($company_id, RepositoryParams $repository_params)
    {
        $users_query = $this->getQueryBuilderWithFields($repository_params);
        $users_query = $this->getQueryBuilderWithWith($users_query, $repository_params);
        $users_query = $this->getQueryBuilderWithSortAndOrder($users_query, $repository_params);
        
        $users = $users_query->whereHas('companies', function ($query) use ($company_id) {
            return $query->where('id', $company_id);
        })->get();

        return $users;
    }
}
