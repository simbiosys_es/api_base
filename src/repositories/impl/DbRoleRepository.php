<?php

namespace Simbiosys\ApiBase\Repository\Impl;

use Simbiosys\ApiBase\Repository\RoleRepositoryInterface;
use Simbiosys\ApiBase\Repository\AbstractRepository;
use Simbiosys\ApiBase\Repository\Helper\RepositoryParams;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use Simbiosys\ApiBase\Model\Role;

class DbRoleRepository extends AbstractRepository implements RoleRepositoryInterface
{
    /**
     * Obtenemos la clase del modelo del repositorio
     *
     * @return mixed
     */
    // phpcs:ignore    
    function model()
    {
        return config('apibase.models.role');
    }

    /**
     * M�todo que devuelve el rol del usuario asociado al identificador unico que se pasa como parametro
     *
     * @param int $user_id
     * @param RepositoryParams $repository_params
     * @throws ModelNotFoundException
     * @return Role
     */
    public function findByUserId($user_id, RepositoryParams $repository_params)
    {
        $role_query = $this->getQueryBuilderWithFields($repository_params);
        $role_query = $this->getQueryBuilderWithWith($role_query, $repository_params);

        $role = $role_query->whereHas('users', function ($query) use ($user_id) {
            return $query->where('id', $user_id);
        })->first();

        if ($role === null) {
            throw new ModelNotFoundException();
        }

        return $role;
    }
}
