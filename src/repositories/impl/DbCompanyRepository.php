<?php

namespace Simbiosys\ApiBase\Repository\Impl;

use Simbiosys\ApiBase\Repository\CompanyRepositoryInterface;
use Simbiosys\ApiBase\Repository\RepositoryInterface;
use Simbiosys\ApiBase\Repository\AbstractRepository;
use Simbiosys\ApiBase\Repository\Helper\RepositoryParams;

use Simbiosys\ApiBase\Model\Company;

class DbCompanyRepository extends AbstractRepository implements CompanyRepositoryInterface
{
    /**
     * Obtenemos la clase del modelo del repositorio
     *
     * @return mixed
     */
    // phpcs:ignore
    function model()
    {
        return config('apibase.models.company');
    }

    /**
     * M�todo que devuelve el rol del usuario asociado al identificador unico que se pasa como parametro
     *
     * @param int $user_id
     * @param RepositoryParams $repository_params
     * @return Company
     */
    public function findByUserId($user_id, RepositoryParams $repository_params)
    {
        $companies_query = $this->getQueryBuilderWithFields($repository_params);
        $companies_query = $this->getQueryBuilderWithWith($companies_query, $repository_params);
        $companies_query = $this->getQueryBuilderWithSortAndOrder($companies_query, $repository_params);
        
        $companies = $companies_query->whereHas('users', function ($query) use ($user_id) {
            return $query->where('id', $user_id);
        })->get();

        return $companies;
    }
}
