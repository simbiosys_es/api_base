<?php

namespace Simbiosys\ApiBase\Repository\Impl;

use Simbiosys\ApiBase\Repository\PermissionRepositoryInterface;
use Simbiosys\ApiBase\Repository\RepositoryInterface;
use Simbiosys\ApiBase\Repository\AbstractRepository;
use Simbiosys\ApiBase\Repository\Helper\RepositoryParams;

use Simbiosys\ApiBase\Model\Permission;

class DbPermissionRepository extends AbstractRepository implements PermissionRepositoryInterface
{
    /**
     * Obtenemos la clase del modelo del repositorio
     *
     * @return mixed
     */
    // phpcs:ignore
    function model()
    {
        return config('apibase.models.permission');
    }
}
