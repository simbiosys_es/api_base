<?php

Route::prefix(config('apibase.route_prefix'))->group(function () {

    Route::post('auth/reset/token', config('apibase.controllers.auth').'@getResetToken');
    Route::post('auth/reset', config('apibase.controllers.reset').'@reset');

     Route::middleware(['auth:api'], config('apibase.api_middlewares'))->group(function () {
        Route::get('me', config('apibase.controllers.user').'@me');
        Route::put('me', config('apibase.controllers.user').'@updateMe');

        Route::resource('users', config('apibase.controllers.user'));
        Route::get('users/{user_id}/companies', config('apibase.controllers.company').'@indexByUserId');
        Route::get('users/{user_id}/roles', config('apibase.controllers.role').'@indexByUserId');
        
        Route::resource('companies', config('apibase.controllers.company'));
        Route::get('companies/{company_id}/users', config('apibase.controllers.user').'@indexByCompanyId');
        
        Route::resource('roles', config('apibase.controllers.role'));
        Route::get('roles/{role_id}/users', config('apibase.controllers.user').'@indexByRoleId');
        
        Route::resource('permissions', config('apibase.controllers.permission'));
    });
});
