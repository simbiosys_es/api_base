<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

// Ignora que no hay namespace, ya que a Laravel no gustan las migraciones con namespace
// phpcs:ignore
class CreateBasePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('apibase.table_names.permissions'), function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('index');
            $table->boolean('show');
            $table->boolean('store');
            $table->boolean('update');
            $table->boolean('destroy');
            $table->string('on', 255);
            $table->string('with', 255);
            $table->integer('id_role');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('apibase.table_names.permissions'));
    }
}
