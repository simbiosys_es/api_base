<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

// Ignora que no hay namespace, ya que a Laravel no gustan las migraciones con namespace
// phpcs:ignore
class CreateBaseCompanyUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('apibase.table_names.companies_users'), function (Blueprint $table) {
            $table->integer('id_company');
            $table->integer('id_user');
            $table->primary(['id_company', 'id_user']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('apibase.table_names.companies_users'));
    }
}
