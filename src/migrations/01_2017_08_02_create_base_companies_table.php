<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

// Ignora que no hay namespace, ya que a Laravel no gustan las migraciones con namespace
// phpcs:ignore
class CreateBaseCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('apibase.table_names.companies'), function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('logo_path')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('contact_email')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('apibase.table_names.companies'));
    }
}
