<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

// Ignora que no hay namespace, ya que a Laravel no gustan las migraciones con namespace
// phpcs:ignore
class AlterPermissionTableOnLimitation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(config('apibase.table_names.permissions'), function (Blueprint $table) {
            $table->renameColumn('with', 'limitation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(config('apibase.table_names.permissions'), function (Blueprint $table) {
            $table->renameColumn('limitation', 'with');
        });
    }
}
