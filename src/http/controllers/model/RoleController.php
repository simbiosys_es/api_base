<?php

namespace Simbiosys\ApiBase\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Simbiosys\ApiBase\Repository\RoleRepositoryInterface;
use Simbiosys\ApiBase\Repository\Helper\RepositoryParams;

use Simbiosys\ApiBase\Http\Controllers\Controller;
use Simbiosys\ApiBase\Http\Resources\ApiResourceCollection;
use Simbiosys\ApiBase\Http\Resources\ApiResource;

use Simbiosys\ApiBase\Http\Requests\RoleIndexRequest;
use Simbiosys\ApiBase\Http\Requests\RoleStoreRequest;
use Simbiosys\ApiBase\Http\Requests\RoleShowRequest;
use Simbiosys\ApiBase\Http\Requests\RoleUpdateRequest;
use Simbiosys\ApiBase\Http\Requests\RoleDestroyRequest;

class RoleController extends AbstractController
{

    public function __construct(RoleRepositoryInterface $role_repository)
    {
        $this->repository = $role_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Simbiosys\ApiBase\Http\Requests\RoleIndexRequest
     */
    public function index(RoleIndexRequest $request)
    {
        return parent::__index($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Simbiosys\ApiBase\Http\Requests\RoleStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleStoreRequest $request)
    {
        return parent::__store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  Simbiosys\ApiBase\Http\Requests\RoleShowRequest  $request
     * @param  int  $role_id
     * @return \Illuminate\Http\Response
     */
    public function show(RoleShowRequest $request, $role_id)
    {
        return parent::__show($request, $role_id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Simbiosys\ApiBase\Http\Requests\RoleUpdateRequest  $request
     * @param  int  $role_id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleUpdateRequest $request, $role_id)
    {
        return parent::__update($request, $role_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Simbiosys\ApiBase\Http\Requests\RoleDestroyRequest  $request
     * @param  int  $role_id
     * @return \Illuminate\Http\Response
     */
    public function destroy(RoleDestroyRequest $request, $role_id)
    {
        return parent::__destroy($role_id);
    }

    /**
     * Listado de roles asociados al usuario cuyo identificador unico se ha proporcionado
     *
     * @param  \Illuminate\Http\RoleIndexRequest  $request
     * @param int $user_id
     * @return \Illuminate\Http\Response
     */
    public function indexByUserId(RoleIndexRequest $request, $user_id)
    {
        try {
            $role = $this->repository->findByUserId($user_id, new RepositoryParams($request->only(['fields', 'with'])));
            return (new ApiResource($role))
                ->response()
                ->setStatusCode(200);
        } catch (ModelNotFoundException $e) {
            return response()
                ->json(["status" => "error", "message" => "Not found"], 404);
        }
    }
}
