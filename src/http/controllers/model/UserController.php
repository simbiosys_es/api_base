<?php

namespace Simbiosys\ApiBase\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Simbiosys\ApiBase\Repository\UserRepositoryInterface;
use Simbiosys\ApiBase\Repository\Helper\RepositoryParams;

use Simbiosys\ApiBase\Http\Requests\UserIndexRequest;
use Simbiosys\ApiBase\Http\Requests\UserStoreRequest;
use Simbiosys\ApiBase\Http\Requests\UserMeRequest;
use Simbiosys\ApiBase\Http\Requests\UserShowRequest;
use Simbiosys\ApiBase\Http\Requests\UserUpdateRequest;
use Simbiosys\ApiBase\Http\Resources\ApiResourceCollection;
use Simbiosys\ApiBase\Http\Requests\UserDestroyRequest;

class UserController extends AbstractController
{

    public function __construct(UserRepositoryInterface $user_repository)
    {
        $this->repository = $user_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(UserIndexRequest $request)
    {
        return parent::__index($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UserStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {
        return parent::__store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $user_id
     * @param  UserShowRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function show(UserShowRequest $request, $user_id)
    {
        return parent::__show($request, $user_id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UserUpdateRequest  $request
     * @param  int  $user_id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $user_id)
    {
        return parent::__update($request, $user_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  UserDestroyRequest  $request
     * @param  int  $user_id
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserDestroyRequest $request, $user_id)
    {
        return parent::__destroy($user_id);
    }

    /**
     * Metodo para obtener la lista de usuarios asociados a un role
     *
     * @param UserIndexRequest $request
     * @param int $role_id
     * @return \Illuminate\Http\Response
     */
    public function indexByRoleId(UserIndexRequest $request, $role_id)
    {
        $users = $this->repository->findByRoleId(
            $role_id,
            new RepositoryParams($request->only(['paginate', 'sort', 'order', 'fields', 'with']))
        );
        return (new ApiResourceCollection($users))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Metodo para obtener la lista de usuarios asociados a una empresa
     *
     * @param UserIndexRequest $request
     * @param int $company_id
     * @return \Illuminate\Http\Response
     */
    public function indexByCompanyId(UserIndexRequest $request, $company_id)
    {
        $users = $this->repository->findByCompanyId(
            $company_id,
            new RepositoryParams($request->only(['paginate', 'sort', 'order', 'fields', 'with']))
        );
        return (new ApiResourceCollection($users))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Devuelve la información del usuario en sesión
     *
     * @param  UserMeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function me(UserMeRequest $request)
    {
        return parent::__show($request, Auth::user()->id);
    }

    /**
     * Permite al usuario en sesión actualizar sus datos
     *
     * @param  UserMeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function updateMe(UserMeRequest $request)
    {
        return parent::__update($request, Auth::user()->id);
    }
}
