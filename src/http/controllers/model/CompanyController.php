<?php

namespace Simbiosys\ApiBase\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Simbiosys\ApiBase\Repository\CompanyRepositoryInterface;
use Simbiosys\ApiBase\Repository\Helper\RepositoryParams;

use Simbiosys\ApiBase\Http\Requests\CompanyIndexRequest;
use Simbiosys\ApiBase\Http\Requests\CompanyStoreRequest;
use Simbiosys\ApiBase\Http\Requests\CompanyShowRequest;
use Simbiosys\ApiBase\Http\Requests\CompanyUpdateRequest;
use Simbiosys\ApiBase\Http\Resources\ApiResourceCollection;
use Simbiosys\ApiBase\Http\Requests\CompanyDestroyRequest;

class CompanyController extends AbstractController
{

    public function __construct(CompanyRepositoryInterface $company_repository)
    {
        $this->repository = $company_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(CompanyIndexRequest $request)
    {
        return parent::__index($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CompanyStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyStoreRequest $request)
    {
        return parent::__store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $company_id
     * @param  CompanyShowRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyShowRequest $request, $company_id)
    {
        return parent::__show($request, $company_id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CompanyUpdateRequest  $request
     * @param  int  $company_id
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyUpdateRequest $request, $company_id)
    {
        return parent::__update($request, $company_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  CompanyDestroyRequest  $request
     * @param  int  $company_id
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyDestroyRequest $request, $company_id)
    {
        return parent::__destroy($company_id);
    }

    /**
     * Metodo para obtener la lista de usuarios asociados a un role
     *
     * @param UserIndexRequest $request
     * @param int $user_id
     * @return \Illuminate\Http\Response
     */
    public function indexByUserId(CompanyIndexRequest $request, $user_id)
    {
        $companies = $this->repository->findByUserId(
            $user_id,
            new RepositoryParams($request->only(['paginate', 'sort', 'order', 'fields', 'with']))
        );
        return (new ApiResourceCollection($companies))
            ->response()
            ->setStatusCode(200);
    }
}
