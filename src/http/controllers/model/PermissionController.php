<?php

namespace Simbiosys\ApiBase\Http\Controllers;

use Illuminate\Http\Request;

use Simbiosys\ApiBase\Repository\PermissionRepositoryInterface;

use Simbiosys\ApiBase\Http\Requests\PermissionIndexRequest;
use Simbiosys\ApiBase\Http\Requests\PermissionStoreRequest;
use Simbiosys\ApiBase\Http\Requests\PermissionShowRequest;
use Simbiosys\ApiBase\Http\Requests\PermissionUpdateRequest;
use Simbiosys\ApiBase\Http\Requests\PermissionDestroyRequest;

class PermissionController extends AbstractController
{

    public function __construct(PermissionRepositoryInterface $permission_repository)
    {
        $this->repository = $permission_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  PermissionIndexRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function index(PermissionIndexRequest $request)
    {
        return parent::__index($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PermissionStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PermissionStoreRequest $request)
    {
        return parent::__store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  PermissionShowRequest  $request
     * @param  int $permission_id
     * @return \Illuminate\Http\Response
     */
    public function show(PermissionShowRequest $request, $permission_id)
    {
        return parent::__show($request, $permission_id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PermissionUpdateRequest $request
     * @param  int  $permission_id
     * @return \Illuminate\Http\Response
     */
    public function update(PermissionUpdateRequest $request, $permission_id)
    {
        return parent::__update($request, $permission_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  PermissionUpdateRequest $request
     * @param  int  $permission_id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PermissionDestroyRequest $request, $permission_id)
    {
        return parent::__destroy($permission_id);
    }
}
