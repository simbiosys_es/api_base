<?php

namespace Simbiosys\ApiBase\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Simbiosys\ApiBase\Repository\Helper\RepositoryParams;
use Simbiosys\ApiBase\Http\Resources\ApiResourceCollection;
use Simbiosys\ApiBase\Http\Resources\ApiResource;

abstract class AbstractController extends BaseController
{

    protected $repository;

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function __index(Request $request)
    {
        $respository_params = new RepositoryParams(
            $request->only(['paginate', 'limit', 'fields', 'with', 'sort', 'order'])
        );

        $elements = $this->repository->getAll($respository_params);
        return (new ApiResourceCollection($elements))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function __store(Request $request)
    {
        $element = $this->repository->create($request->all());
        return (new ApiResource($element))
            ->response()
            ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int $element_id
     * @return \Illuminate\Http\Response
     */
    protected function __show(Request $request, $element_id)
    {
        $repository_params = new RepositoryParams($request->only(['with', 'fields']));

        try {
            $element = $this->repository->findById($element_id, $repository_params);
            return (new ApiResource($element))
                ->response()
                ->setStatusCode(200);
        } catch (ModelNotFoundException $e) {
            return response()
                ->json(["status" => "error", "message" => "Not found"], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $element_id
     * @return \Illuminate\Http\Response
     */
    protected function __update(Request $request, $element_id)
    {
        try {
            $element = $this->repository->updateById($element_id, $request->all());
            return (new ApiResource($element))
                ->response()
                ->setStatusCode(200);
        } catch (ModelNotFoundException $e) {
            return response()
                ->json(["status" => "error", "message" => "Not found"], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $element_id
     * @return \Illuminate\Http\Response
     */
    protected function __destroy($element_id)
    {
        $deleted_element = $this->repository->deleteById($element_id);
        if ($deleted_element == 0) {
            return response()
                ->json(["status" => "error", "message" => "Not found"], 404);
        }

        return response()
            ->json([], 204);
    }
}
