<?php

namespace Simbiosys\ApiBase\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class BaseAuthController extends Controller
{
    use SendsPasswordResetEmails;

    public function getResetToken(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'email' => 'required|email|exists:'.config('apibase.table_names.users')
            ]
        );

        //si falla alguna validacion, devolvemos el error/es correspondientes
        if ($validator->fails()) {
            return response()->json(['errors' => ['code' => 400, 'message' => $validator->errors()]], 400);
        }

        $user = call_user_func([config('apibase.models.user'), 'where'], ['email' => $request->get('email')])->first();
        if (!$user) {
            return response()->json(
                [
                    'errors' => [
                        'code'=> 404,
                        'message' => 'No se encuentra a ningun usuario con ese correo.'
                    ]
                ],
                404
            );
        }

        $token = $this->broker()->createToken($user);
        $user->sendPasswordResetNotification($token);
        return response()->json([ 'status' => 'ok', 'message' => [ 'token' => $token ] ], 200);
    }
}
