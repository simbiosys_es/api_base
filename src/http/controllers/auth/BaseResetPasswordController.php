<?php

namespace Simbiosys\ApiBase\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Simbiosys\ApiBase\Http\Requests\PasswordResetRequest;

class BaseResetPasswordController extends Controller
{

    use ResetsPasswords;
    
    /*
    * email [your-email-address]
    * password [new-password]
    * password_confirmation [retype-new-password]
    * token [the-token-you-get-from-previous-one]
    */
    public function reset(PasswordResetRequest $request)
    {
        $response = $this->broker()->reset(
            $this->credentials($request),
            function ($user, $password) {
                $this->resetPassword($user, $password);
            }
        );

        if ($response == Password::PASSWORD_RESET) {
            return response()->json(['status' => 'ok'], 200);
        } else {
            return response()->json(['status' => 'error'], 202);
        }
    }

    protected function resetPassword($user, $password)
    {
        $user->forceFill([
            'password' => $password,
            'remember_token' => '',
            'api_token' => ''
        ])->save();
    }
}
