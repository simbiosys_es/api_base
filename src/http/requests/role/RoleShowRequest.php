<?php

namespace Simbiosys\ApiBase\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

use Simbiosys\ApiBase\Model\Role;

class RoleShowRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check() && Auth::user()->role->hasPermission('roles', 'show');
    }

    /**
     * Return the model string class
     *
     * @return String
     */
    private function getModelStringClass()
    {
        return config('apibase.models.role');
    }

    /**
     * Return a new Role from the Role class on api base configuration
     *
     * @return Role
     */
    private function getModelInstance()
    {
        return (new \ReflectionClass($this->getModelStringClass()))->newInstance();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fields' => function ($attribute, $value, $fail) {
                collect(explode(",", $value))->each(function ($value) use ($fail, $attribute) {
                    if (!in_array($value, $this->getModelInstance()->getFillable())) {
                        return $fail('The selected '. $attribute.' is invalid.');
                    }
                });
            },
            'with' => function ($attribute, $value, $fail) {
                collect(explode(",", $value))->each(function ($value) use ($fail, $attribute) {
                    if (!in_array($value, constant($this->getModelStringClass().'::RELATIONSHIPS'))) {
                        return $fail('The selected '. $attribute.' is invalid.');
                    }
                });
            }
        ];
    }
}
