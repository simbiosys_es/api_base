<?php

namespace Simbiosys\ApiBase\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\CanResetPassword;

use Laravel\Passport\HasApiTokens;

use Simbiosys\ApiBase\Auth\Notifications\ResetPasswordNotification;

class User extends Authenticatable implements CanResetPassword
{
    use Notifiable, SoftDeletes, HasApiTokens;

    public const RELATIONSHIPS = ['role', 'companies'];

    protected $guarded = ['id'];
    protected $fillable = ['id', 'name', 'email', 'id_role', 'created_at', 'updated_at', 'deleted_at', 'password'];
    protected $hidden = [
        'api_token', 'password', 'remember_token',
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        //Configurar el nombre de la tabla
        $this->setTable(config('apibase.table_names.users'));
    }

    /*
    * Relacion con empresas
    */
    public function companies()
    {
        return $this->belongsToMany(
            config('apibase.models.company'),
            config('apibase.table_names.companies_users'),
            'id_user',
            'id_company'
        )->withTimestamps();
    }

    /*
    * Relacion con el rol
    */
    public function role()
    {
        return $this->belongsTo(config('apibase.models.role'), 'id_role', 'id');
    }
    
    /*
    * Encriptar contraseña
    */
    public function setPasswordAttribute($password)
    {
        if (strlen($password) > 0) {
            $this->attributes['password'] = bcrypt($password);
        }
    }

    public function getGuarded()
    {
        return $this->guarded;
    }

    public function scopeByUser($query, $user)
    {
        $permission = $user
            ->role
            ->permissions()
            ->where('on', 'users')
            ->first();

        switch ($permission->limitation) {
            case 'user':
                return $query->where('id', $user->id);
            default:
                return $query;
        }
    }

    public function getEmailForPasswordReset()
    {
        return $this->email;
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
}
