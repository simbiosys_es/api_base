<?php

namespace Simbiosys\ApiBase\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;

    public const RELATIONSHIPS = ['users', 'permissions'];

    protected $guarded = ['id'];
    protected $fillable = ['id', 'name', 'created_at', 'updated_at', 'deleted_at'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        //Configurar el nombre de la tabla
        $this->setTable(config('apibase.table_names.roles'));
    }

    public function getGuarded()
    {
        return $this->guarded;
    }

    /*
    * Relacion con el rol
    */
    public function users()
    {
        return $this->hasMany(config('apibase.models.user'), 'id_role', 'id');
    }

    /*
    * Relacion con los permisos
    */
    public function permissions()
    {
        return $this->hasMany(config('apibase.models.permission'), 'id_role', 'id');
    }

    /**
     * Metodo para comprobar si un rol tiene permiso sobre cirta ruta
     *
     * @param string $element nombre de la ruta
     * @param string $method nombre del metodo (index,store,update,destroy)
     * @return boolean
     */
    public function hasPermission($element, $method)
    {
        $permission = $this->permissions()->where(['on' => $element, $method => 1])->first();
        return $permission !== null;
    }

    public function scopeByUser($query, $user)
    {
        $permission = $user
            ->role
            ->permissions()
            ->where('on', 'roles')
            ->first();

        switch ($permission->limitation) {
            case 'all':
                return $query;
            default:
                return $query->where('id', '-1');
        }
    }
}
