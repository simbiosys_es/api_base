<?php

namespace Simbiosys\ApiBase\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Simbiosys\ApiBase\Traits\Search;

class Permission extends Model
{
    use SoftDeletes;

    public const RELATIONSHIPS = ['role'];

    protected $guarded = ["id"];
    protected $fillable = ['id', 'index', 'show', 'store', 'update', 'destroy', 'on', 'limitation', 'id_role'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        //Configurar el nombre de la tabla
        $this->setTable(config('apibase.table_names.permissions'));
    }

    /*
    * Relacion con el rol
    */
    public function role()
    {
        return $this->belongsTo(config('apibase.models.role'), 'id_role', 'id');
    }

    public function scopeByUser($query, $user)
    {
        $permission = $user
            ->role
            ->permissions()
            ->where('on', 'permissions')
            ->first();

        switch ($permission->limitation) {
            case 'all':
                return $query;
        }
    }
}
