<?php

namespace Simbiosys\ApiBase\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use SoftDeletes;

    public const RELATIONSHIPS = ['users'];

    protected $guarded = ['id'];
    protected $fillable = ['id', 'name', 'logo_path', 'contact_number', 'contact_email'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        //Configurar el nombre de la tabla
        $this->setTable(config('apibase.table_names.companies'));
    }

    /*
    * Relacion con usuarios
    */
    public function users()
    {
        return $this->belongsToMany(
            config('apibase.models.user'),
            config('apibase.table_names.companies_users'),
            'id_company',
            'id_user'
        )->withTimestamps();
    }

    public function scopeByUser($query, $user)
    {
        $permission = $user
            ->role
            ->permissions()
            ->where('on', 'companies')
            ->first();

        switch ($permission->limitation) {
            case 'all':
                return $query;
            case 'company':
                return $query->whereIn('id', $_id);
            case 'user':
                return $query->whereHas('users', function ($query) use ($_id) {
                    return $query->where('id', $_id);
                });
        }
    }
}
